<%@ include file="../common/IncludeTop.jsp"%>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    #tab{
        text-align: center;
        list-style: none;
    }
    #tab li {
        display: inline-block;
        list-style: none;
        cursor: pointer;
        padding: 0.8ex;
        margin-top: 1.5ex;
        background: #eff1ee no-repeat;
        font: bold 27px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        color: #4f7258;
        border-right: 1px solid #C1DAD7;
        border-bottom: 1px solid #C1DAD7;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
    }

    .hide {
        display: none;
    }

</style>
<script src="js/jquery-3.3.1.js"></script>
<script>
    $(function () {
        //click事件里面会有for循环，为每个设置点击事件
        $("#tab li").click(function () {
            $("#container div").eq($(this).index()).removeClass("hide").siblings().addClass("hide");
        });
    });
</script>
</head>
<body>

<ul id="tab">
    <li id="tab1" style="width: 300px">New Order</li>
    <li id="tab2" style="width: 0"></li>
    <li id="tab3" style="width: 300px">Shipping Order</li>
</ul>

<div id="container">
    <div id="content1">
        <div id="Catalog"><form action="ConfirmOrder" method="post">
            <style type="text/css">

                table {
                    width: 700px;
                    padding: 0;
                    margin: 0;
                }

                caption {
                    padding: 0 0 5px 0;
                    width: 700px;
                    font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                    text-align: right;
                }

                th {
                    font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                    color: #4f6b72;
                    border-right: 1px solid #C1DAD7;
                    border-bottom: 1px solid #C1DAD7;
                    border-top: 1px solid #C1DAD7;
                    letter-spacing: 2px;
                    text-transform: uppercase;
                    text-align: left;
                    padding: 6px 6px 6px 12px;
                    background: #d5eaca no-repeat;
                }
                td {
                    border-right: 1px solid #C1DAD7;
                    border-bottom: 1px solid #C1DAD7;
                    background: #fff;
                    font-size:16px;
                    padding: 6px 6px 6px 12px;
                    color: #4f6b72;
                }

            </style>
            <table>
                <tr>
                    <th colspan=2>Payment Details</th>
                </tr>
                <tr>
                    <td>Card Type:</td>
                    <td><select name="cardType">
                        <option value="Visa" selected="selected">Visa</option>
                        <option value="MasterCard">MasterCard</option>
                        <option value="American Express">American Express</option>
                    </select>
                </tr>
                <tr>
                    <td>Card Number:</td>
                    <td><input type="text" name="creditCard" value=${sessionScope.order.creditCard}> </td>
                </tr>
                <tr>
                    <td>Expiry Date (MM/YYYY):</td>
                    <td><input type="text"  name="expiryDate" value=${sessionScope.order.expiryDate}></td>
                </tr>
                <tr>
                    <th colspan=2>Billing Address</th>
                </tr>

                <tr>
                    <td>First name:</td>
                    <td><input type="text" name="billToFirstName" value=${sessionScope.order.billToFirstName}></td>
                </tr>
                <tr>
                    <td>Last name:</td>
                    <td><input type="text" name="billToLastName" value=${sessionScope.order.billToLastName}></td>
                </tr>
                <tr>
                    <td>Address 1:</td>
                    <td><input type="text" size="40" name="billAddress1" value=${sessionScope.order.billAddress1}></td>
                </tr>
                <tr>
                    <td>Address 2:</td>
                    <td><input type="text" size="40" name="billAddress2" value=${sessionScope.order.billAddress2}></td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><input type="text" name="billCity" value=${sessionScope.order.billCity}></td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td><input type="text" size="4" name="billState" value=${sessionScope.order.billState}></td>
                </tr>
                <tr>
                    <td>Zip:</td>
                    <td><input type="text" size="10" name="billZip" value=${sessionScope.order.billZip}></td>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><input type="text"  size="15" name="billCountry" value=${sessionScope.order.billCountry}></td>
                </tr>

            </table>

            <input type="submit"  name="newOrder" value="Continue" />

        </form></div>
    </div>
    <div id="content2" class="hide">
        <div id="Catalog">
            <form action="ShippingAddress" method="post">
                <table>
                    <tr>
                        <th colspan=2>Shipping Address</th>
                    </tr>

                    <tr>
                        <td>First name:</td>
                        <td><input type="text" name="shipToFirstName" value="${sessionScope.order.shipToFirstName}"/></td>
                    </tr>
                    <tr>
                        <td>Last name:</td>
                        <td><input type="text" name="shipToLastName" value="${sessionScope.order.shipToLastName}"/></td>
                    </tr>
                    <tr>
                        <td>Address 1:</td>
                        <td><input type="text" size="40" name="shipAddress1" value="${sessionScope.order.shipAddress1}"/></td>
                    </tr>
                    <tr>
                        <td>Address 2:</td>
                        <td><input type="text" size="40" name="shipAddress2" value="${sessionScope.order.shipAddress2}"/></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><input type="text" name="shipCity" value="${sessionScope.order.shipCity}"/></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td><input type="text" size="4" name="shipState" value="${sessionScope.order.shipState}"/></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td><input type="text" size="10" name="shipZip" value="${sessionScope.order.shipZip}"/></td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td><input type="text" size="15" name="shipCountry" value="${sessionScope.order.shipCountry}"/></td>
                    </tr>

                </table>
                <input type="submit" name="newOrder" value="Continue"/>
            </form>
        </div>
    </div>


<%@ include file="../common/IncludeBottom.jsp"%>