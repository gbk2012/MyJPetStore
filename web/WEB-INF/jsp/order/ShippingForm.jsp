<%@ include file="../common/IncludeTop.jsp"%>

<div id="Catalog">
	<form action="ShippingAddress" method="post">
		<style type="text/css">

			table {
				width: 700px;
				padding: 0;
				margin: 0;
			}

			caption {
				padding: 0 0 5px 0;
				width: 700px;
				font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
				text-align: right;
			}

			th {
				font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
				color: #4f6b72;
				border-right: 1px solid #C1DAD7;
				border-bottom: 1px solid #C1DAD7;
				border-top: 1px solid #C1DAD7;
				letter-spacing: 2px;
				text-transform: uppercase;
				text-align: left;
				padding: 6px 6px 6px 12px;
				background: #d5eaca no-repeat;
			}

			th.nobg {
				border-top: 0;
				border-left: 0;
				border-right: 1px solid #C1DAD7;
				background: none;
			}

			td {
				border-right: 1px solid #C1DAD7;
				border-bottom: 1px solid #C1DAD7;
				background: #fff;
				font-size:16px;
				padding: 6px 6px 6px 12px;
				color: #4f6b72;
			}


			td.alt {
				background: #F5FAFA;
				color: #797268;
			}

			th.spec {
				border-left: 1px solid #C1DAD7;
				border-top: 0;
				background: #fff no-repeat;
				font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			}

			th.specalt {
				border-left: 1px solid #C1DAD7;
				border-top: 0;
				background: #f5fafa no-repeat;
				font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
				color: #797268;
			}
		</style>
		<table>
			<tr>
				<th colspan=2>Shipping Address</th>
			</tr>

			<tr>
				<td>First name:</td>
				<td><input type="text" name="shipToFirstName" value="${sessionScope.order.shipToFirstName}"/></td>
			</tr>
			<tr>
				<td>Last name:</td>
				<td><input type="text" name="shipToLastName" value="${sessionScope.order.shipToLastName}"/></td>
			</tr>
			<tr>
				<td>Address 1:</td>
				<td><input type="text" size="40" name="shipAddress1" value="${sessionScope.order.shipAddress1}"/></td>
			</tr>
			<tr>
				<td>Address 2:</td>
				<td><input type="text" size="40" name="shipAddress2" value="${sessionScope.order.shipAddress2}"/></td>
			</tr>
			<tr>
				<td>City:</td>
				<td><input type="text" name="shipCity" value="${sessionScope.order.shipCity}"/></td>
			</tr>
			<tr>
				<td>State:</td>
				<td><input type="text" size="4" name="shipState" value="${sessionScope.order.shipState}"/></td>
			</tr>
			<tr>
				<td>Zip:</td>
				<td><input type="text" size="10" name="shipZip" value="${sessionScope.order.shipZip}"/></td>
			</tr>
			<tr>
				<td>Country:</td>
				<td><input type="text" size="15" name="shipCountry" value="${sessionScope.order.shipCountry}"/></td>
			</tr>

		</table>
		<input type="submit" name="newOrder" value="Continue"/>
	</form>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>