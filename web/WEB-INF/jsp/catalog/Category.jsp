<%@ include file="../common/IncludeTop.jsp"%>
<div id="BackLink">
    <a href="main">
        Return to Main Menu</a></div>

<div id="Catalog">

    <h2>${sessionScope.category.name}</h2>

    <style type="text/css">

    table {
    width: 700px;
    padding: 0;
    margin: 0;
    }

    caption {
    padding: 0 0 5px 0;
    width: 700px;
    font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
    text-align: right;
    }

    th {
    font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
    color: #4f6b72;
    border-right: 1px solid #C1DAD7;
    border-bottom: 1px solid #C1DAD7;
    border-top: 1px solid #C1DAD7;
    letter-spacing: 2px;
    text-transform: uppercase;
    text-align: left;
    padding: 6px 6px 6px 12px;
    background: #d5eaca no-repeat;
    }

    th.nobg {
    border-top: 0;
    border-left: 0;
    border-right: 1px solid #C1DAD7;
    background: none;
    }

    td {
    border-right: 1px solid #C1DAD7;
    border-bottom: 1px solid #C1DAD7;
    background: #fff;
    font-size:16px;
    padding: 6px 6px 6px 12px;
    color: #4f6b72;
    }


    td.alt {
    background: #F5FAFA;
    color: #797268;
    }

    th.spec {
    border-left: 1px solid #C1DAD7;
    border-top: 0;
    background: #fff no-repeat;
    font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
    }

    th.specalt {
    border-left: 1px solid #C1DAD7;
    border-top: 0;
    background: #f5fafa no-repeat;
    font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
    color: #797268;
    }
    </style>
    <table>
        <tr>
            <th>Product ID</th>
            <th>Name</th>
        </tr>
        <c:forEach var="product" items="${sessionScope.productList}">
            <tr>
                <td><a href="ViewProduct?productId=${product.productId}&username=${sessionScope.username}&category=${sessionScope.category}">${product.productId}</a> </td>
                <td>${product.name}</td>
            </tr>
        </c:forEach>
    </table>

</div>
<%@ include file="../common/IncludeBottom.jsp"%>
