
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/IncludeTop.jsp"%>
<script type="text/javascript" src="js/tools.js"></script>
<script type="text/javascript">
    window.onload = function (){
        var imgList = document.getElementById("imgList");
        var imgArr = document.getElementsByTagName("img");
        imgList.style.width = 460*imgArr.length+"px";

        //获取所有的a
        var index = 0;
        var allA = document.getElementsByClassName("dian");
        allA[index].style.backgroundColor = "#005e21";
        for (var i=0;i<allA.length;i++){
            allA[i].num = i;
            allA[i].onclick = function (){
                index = this.num;
                imgList.style.left = -460*index+"px";
                setA();
                //move(imgList, "left", -460*index, 25, function(){});
            };
        }

        autoChange();
        //创建方法设置选中的a
        function setA(){
            for (var i=0;i<allA.length;i++){
                allA[i].style.backgroundColor = "";
            }
            allA[index].style.backgroundColor = "005e21";
        };

        //自动切换
        function autoChange(){
            setInterval(function(){
                index++;
                if(index>4){//当n>2的时候，n=0;也就是将第一章图片归位回到第一位
                    index=0;
                }
                imgList.style.left = -460*index+"px";
                setA();
            },2000);
        }

    };
</script>
    <div id="Welcome" style="text-align: center">
        <div id="WelcomeContent">
            Welcome to MyJPetStore!
        </div>
    </div>

    <div id="Main" class="container">
        <div id="Sidebar" class="timeline" style="border-style:inset;margin: 5px 35px;margin-bottom:20px;border:1px solid #92B0DD;background-color: #c0ebd7">
            <div id="SidebarContent">
                <div> <a href="ViewCategory?categoryId=FISH&username=${sessionScope.username}"><img src="images/fish_icon.gif" /></a>
                <br/> Saltwater, Freshwater <br/>
                </div>
                <div><a href="ViewCategory?categoryId=DOGS&username=${sessionScope.username}"><img src="images/dogs_icon.gif" /></a>
                <br /> Various Breeds <br />
                </div>
                <div><a href="ViewCategory?categoryId=CATS&username=${sessionScope.username}"><img src="images/cats_icon.gif" /></a>
                <br /> Various Breeds, Exotic Varieties <br />
                </div>
                <div><a href="ViewCategory?categoryId=REPTILES&username=${sessionScope.username}"><img src="images/reptiles_icon.gif" /></a>
                <br /> Lizards, Turtles, Snakes <br />
                </div>
                <div><a href="ViewCategory?categoryId=BIRDS&username=${sessionScope.username}"><img src="images/birds_icon.gif" /></a>
                <br /> Exotic Varieties
                </div>
            </div>
        </div>

        <div id="Promo">
            <!--动态图片-->
            <ul id="imgList">
                <li><img src="images/pet1.jpg"></li>
                <li><img src="images/pet20.jpg"></li>
                <li><img src="images/pet30.jpg"></li>
                <li><img src="images/pet40.jpg"></li>
                <li><img src="images/pet50.jpg"></li>
            </ul>
            <span class="prev">&lt; </span>
            <span class="next">&gt; </span>
            <div id="navDiv">
                <a href="javascript:;" class="dian"></a>
                <a href="javascript:;" class="dian"></a>
                <a href="javascript:;" class="dian"></a>
                <a href="javascript:;" class="dian"></a>
                <a href="javascript:;" class="dian"></a>
            </div>
        </div>

        <div id="toukui">
            <img src="images/erweima.jpg">
        </div>

        <div id="inform" style="display: none">sssssssssss</div>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/mouseEvent.js"></script>
   <div id="zhong">
        <div id="MainImage">
            <div id="MainImageContent">
                <!--中间显示栏-->
                <map name="estoremap">
                    <area alt="BIRDS" coords="72,2,280,250" href="ViewCategory?categoryId=BIRDS" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                    <area alt="FISH" coords="2,180,72,250" href="ViewCategory?categoryId=FISH" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                    <area alt="DOGS" coords="60,250,130,320" href="ViewCategory?categoryId=DOGS" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                    <area alt="REPTILES" coords="140,270,210,340" href="ViewCategory?categoryId=REPTILES" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                    <area alt="CATS" coords="225,240,295,310" href="ViewCategory?categoryId=CATS" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                    <area alt="BIRDS" coords="280,180,350,250" href="ViewCategory?categoryId=BIRDS" shape="rect"
                          onmouseover="showInform(alt);" onmouseout="hiddenInform(event)"/>
                </map>
                <img height="355" src="images/splash.gif" align="middle" usemap="#estoremap" width="400" />
            </div>
        </div>
        <div id="Tips">
            <div id="first">
                <ul>
                    <li>
                        <h2><img src="images/phone.png"></h2>
                        <div>
                            <h4>Telephone Consultation</h4>
                            <p>Phone:8888-8080</p>
                        </div>
                    </li>
                    <li>
                        <h2><img src="images/sales.png"></h2>
                        <div>
                            <h4>Quality Assurance</h4>
                            <P>Imported pet, superior quality</P>
                        </div>
                    </li>
                </ul>
            </div>
            <div id="second">
                <h5>Address: Railway College of Central South University, 68 Shaoshan South Road, Wenyuan street, Tianxin District, Changsha City, Hunan Province</h5>
            </div>
        </div>


        </div>
   </div>

        <div id="Separator">&nbsp;</div>
</div>

<%@include file="../common/IncludeBottom.jsp"%>