<%@ include file="../common/IncludeTop.jsp"%>

<div id="Catalog"><form action="UpdateInformation" method="post">

	<h3>User Information</h3>
	<style type="text/css">

		table {
			width: 700px;
			padding: 0;
			margin: 0;
		}

		caption {
			padding: 0 0 5px 0;
			width: 700px;
			font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			text-align: right;
		}

		th {
			font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			color: #4f6b72;
			border-right: 1px solid #C1DAD7;
			border-bottom: 1px solid #C1DAD7;
			border-top: 1px solid #C1DAD7;
			letter-spacing: 2px;
			text-transform: uppercase;
			text-align: left;
			padding: 6px 6px 6px 12px;
			background: #d5eaca no-repeat;
		}

		th.nobg {
			border-top: 0;
			border-left: 0;
			border-right: 1px solid #C1DAD7;
			background: none;
		}

		td {
			border-right: 1px solid #C1DAD7;
			border-bottom: 1px solid #C1DAD7;
			background: #fff;
			font-size:16px;
			padding: 6px 6px 6px 12px;
			color: #4f6b72;
		}


		td.alt {
			background: #F5FAFA;
			color: #797268;
		}

		th.spec {
			border-left: 1px solid #C1DAD7;
			border-top: 0;
			background: #fff no-repeat;
			font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		}

		th.specalt {
			border-left: 1px solid #C1DAD7;
			border-top: 0;
			background: #f5fafa no-repeat;
			font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			color: #797268;
		}
	</style>
	<table>
		<tr>
			<td style="background: #d5eaca no-repeat;">User ID: </td>
			<td><input type="text" name="username"  value="${account.username}"></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">New password:</td>
			<td><input type="password"  name="password"  value="${account.password}"></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Repeat password:</td>
			<td><input type="text"  name="repeatedPassword" ></td>
		</tr>
	</table>

	<h3>Account Information</h3>

	<table>
		<tr>
			<td style="background: #d5eaca no-repeat;">First name:</td>
			<td><input type="text" name="firstName" value=${account.firstName} ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Last name:</td>
			<td><input type="text"  id="lastname" name="lastName" value=${account.lastName} ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Email:</td>
			<td><input type="text" size="40" id="email" name="email" value=${account.email}></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Phone:</td>
			<td><input type="text"  id="phone" name="phone"value=${account.phone} ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Address 1:</td>
			<td><input type="text"  size="40" id="address1" name="address1" value=${account.address1}></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Address 2:</td>
			<td><input type="text" size="40" id="adderss2" name="address2" value=${account.address2}></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">City:</td>
			<td><input type="text"  id="city" name="city" value=${account.city} ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">State:</td>
			<td><input type="text"size="4" id="state" name="state" value=${account.state}></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Zip:</td>
			<td><input type="text"size="10" id="zip" name="zip" value=${account.zip} ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Country:</td>
			<td><input type="text" size="15" id="country" name="country" value=${account.country} ></td>
		</tr>
	</table>

	<h3>Profile Information</h3>

	<table>
		<tr>
			<td style="background: #d5eaca no-repeat;">Language Preference:</td>
			<td>
				<select name="languagePreference" id="langPr">
					<option value="english" selected="selected">english</option>
					<option value="japanese">japanese</option>

				</select>
			</td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Favourite Category:</td>
			<td>
				<select name="favouriteCategoryId" id="favocate">
					<option value="FISH">FISH</option>
					<option selected="selected" value="DOGS">DOGS</option>
					<option value="REPTILES">REPTILES</option>
					<option value="CATS">CATS</option>
					<option value="BIRDS">BIRDS</option>

				</select>
			</td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Enable MyList</td>
			<td><input type="checkbox"  name="listOption" value="${sessionScope.account.listOption}" ></td>
		</tr>
		<tr>
			<td style="background: #d5eaca no-repeat;">Enable MyBanner</td>
			<td><input type="checkbox" name="bannerOption" value="${sessionScope.account.bannerOption}"></td>
		</tr>
	</table>

	</table>

	<input type="submit"  name="editAccount" value="Save Account Information" />

</form>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>
