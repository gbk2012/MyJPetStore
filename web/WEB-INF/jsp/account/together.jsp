<%@ include file="../common/IncludeTop.jsp"%>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    #tab{
        text-align: center;
        list-style: none;
    }
    #tab li {
        display: inline-block;
        list-style: none;
        cursor: pointer;
        padding: 0.8ex;
        margin-top: 1.5ex;
        background: #eff1ee no-repeat;
        font: bold 27px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        color: #4f7258;
        border-right: 1px solid #C1DAD7;
        border-bottom: 1px solid #C1DAD7;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
    }

    .hide {
        display: none;
    }

</style>
<body>

<ul id="tab">
    <li id="tab1" style="width: 300px">User Infor</li>
    <li id="tab3" style="width: 300px;display: none">Profile Infor</li>
</ul>

<div id="container">
    <form action="SaveInformation" method="post">
    <div id="content1">
        <div id="Catalog">
            <style type="text/css">

                table {
                    width: 700px;
                    padding: 0;
                    margin: 0;
                }

                caption {
                    padding: 0 0 5px 0;
                    width: 700px;
                    font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                    text-align: right;
                }
                th {
                    font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
                    color: #4f6b72;
                    border-right: 1px solid #C1DAD7;
                    border-bottom: 1px solid #C1DAD7;
                    border-top: 1px solid #C1DAD7;
                    letter-spacing: 2px;
                    text-transform: uppercase;
                    text-align: left;
                    padding: 6px 6px 6px 12px;
                    background: #d5eaca no-repeat;
                }
                td {
                    border-right: 1px solid #C1DAD7;
                    border-bottom: 1px solid #C1DAD7;
                    background: #fff;
                    font-size:16px;
                    padding: 6px 6px 6px 12px;
                    color: #4f6b72;
                }

            </style>
            <table>
                <tr>
                    <td style="background: #d5eaca no-repeat;">User ID:</td>
                    <td><input type="text"  name="username" id = "username" onblur="usernameIsExist();">
                        <div id="usernameMsg"></div><script type="text/javascript" src="${pageContext.request.contextPath }/js/register.js"></script>
                    </td>

                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">New password:</td>
                    <td><input id="pw" name="password"type="password" onblur="CheckIntensity(this.value)"></td>
                </tr>
                <tr  id="strong" style="display: none">
                    <td id="pwd_Weak" class="pwd pwd_c"> </td>
                    <td id="pwd_Medium" class="pwd pwd_c pwd_f"></td>
                    <td id="pwd_Strong" class="pwd pwd_c pwd_c_r"> </td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">Repeat password:</td>
                    <td><input name="repeatedPassword" id="repeatedPassword" type="password" onblur="checkpw()">
                        <div id="checkpw" style="display:none"></div>
                        <div id="error"></div>
                        <script type="text/javascript" src="${pageContext.request.contextPath }/js/repeatpassword.js"></script>
                    </td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">VerificationCode:</td>
                    <td>
                        <input type="text" name="vCode" size="5" maxlength="4"/>
                        <a href="newAccount"><img border="0" src="VerificationCode" name="checkcode"></a>
                    </td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">Language Preference:</td>
                    <td>
                        <select name="languagePreference" id="langPr">
                            <option value="english" selected="selected">english</option>
                            <option value="japanese">japanese</option>
                        </select>

                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">Favourite Category:</td>
                    <td>
                        <select name="favouriteCategoryId" id="favocate">
                            <option value="FISH">FISH</option>
                            <option selected="selected" value="DOGS">DOGS</option>
                            <option value="REPTILES">REPTILES</option>
                            <option value="CATS">CATS</option>
                            <option value="BIRDS">BIRDS</option>

                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">Enable MyList</td>
                    <td><input type="checkbox"  name="listOption" value="${sessionScope.account.listOption}" ></td>
                </tr>
                <tr>
                    <td style="background: #d5eaca no-repeat;">Enable MyBanner</td>
                    <td><input type="checkbox" name="bannerOption" value="${sessionScope.account.bannerOption}"></td>
                </tr>
            </table>
            <input type="button"  name="newAccount" onclick="nextPage()" value="Next" />
            <script type="text/javascript" src="${pageContext.request.contextPath }/js/nextPage.js"></script>
        </div>
    </div>

    <div id="content2" class="hide">
        <div id="Catalog">
                <table>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">First name:</td>
                        <td><input type="text"  name="firstname"  id="firstname" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Last name:</td>
                        <td><input type="text"  name="lastname"  id="lastname"  ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Email:</td>
                        <td><input type="text" size="40" id="email" name="email" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Phone:</td>
                        <td><input type="text"  id="phone" name="phone" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Address 1:</td>
                        <td><input type="text"  size="40" id="address1" name="address1" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Address 2:</td>
                        <td><input type="text" size="40" id="adderss2" name="address2" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">City:</td>
                        <td><input type="text"  id="city" name="city" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">State:</td>
                        <td><input type="text"size="4" id="state" name="state"></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Zip:</td>
                        <td><input type="text"size="10" id="zip" name="zip" ></td>
                    </tr>
                    <tr>
                        <td style="background: #d5eaca no-repeat;">Country:</td>
                        <td><input type="text" size="15" id="country" name="country" ></td>
                    </tr>
                </table>
                <input type="button" name="return" id="return" onclick="returnPage()" value="Return">
                <script type="text/javascript" src="${pageContext.request.contextPath }/js/returnPage.js"></script>
                <input type="submit" name="newOrder" value="Register"/>
        </div>
    </div>
    </form>
</div>


<%@ include file="../common/IncludeBottom.jsp"%>