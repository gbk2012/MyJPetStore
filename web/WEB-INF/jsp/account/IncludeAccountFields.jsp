<h3>Account Information</h3>
<style type="text/css">

	table {
		width: 700px;
		padding: 0;
		margin: 0;
	}

	caption {
		padding: 0 0 5px 0;
		width: 700px;
		font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		text-align: right;
	}

	th {
		font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		color: #4f6b72;
		border-right: 1px solid #C1DAD7;
		border-bottom: 1px solid #C1DAD7;
		border-top: 1px solid #C1DAD7;
		letter-spacing: 2px;
		text-transform: uppercase;
		text-align: left;
		padding: 6px 6px 6px 12px;
		background: #d5eaca no-repeat;
	}

	th.nobg {
		border-top: 0;
		border-left: 0;
		border-right: 1px solid #C1DAD7;
		background: none;
	}

	td {
		border-right: 1px solid #C1DAD7;
		border-bottom: 1px solid #C1DAD7;
		background: #fff;
		font-size:16px;
		padding: 6px 6px 6px 12px;
		color: #4f6b72;
	}


	td.alt {
		background: #F5FAFA;
		color: #797268;
	}

	th.spec {
		border-left: 1px solid #C1DAD7;
		border-top: 0;
		background: #fff no-repeat;
		font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
	}

	th.specalt {
		border-left: 1px solid #C1DAD7;
		border-top: 0;
		background: #f5fafa no-repeat;
		font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		color: #797268;
	}
</style>
<table>
	<tr>
		<td>First name:</td>
		<td><input type="text"  value=${account.firstName} ></td>
	</tr>
	<tr>
		<td>Last name:</td>
		<td><input type="text"  id="lastname" value=${account.lastName} ></td>
	</tr>
	<tr>
		<td>Email:</td>
		<td><input type="text" size="40" id="email" value=${account.email} ></td>
	</tr>
	<tr>
		<td>Phone:</td>
		<td><input type="text"  id="phone" value=${account.phone} ></td>
	</tr>
	<tr>
		<td>Address 1:</td>
		<td><input type="text"  size="40" id="address1" value=${account.address1} ></td>
	</tr>
	<tr>
		<td>Address 2:</td>
		<td><input type="text" size="40" id="adderss2" value=${account.address2} ></td>
	</tr>
	<tr>
		<td>City:</td>
		<td><input type="text"  id="city" value=${account.city} ></td>
	</tr>
	<tr>
		<td>State:</td>
		<td><input type="text"size="4" id="state" value=${account.state}></td>
	</tr>
	<tr>
		<td>Zip:</td>
		<td><input type="text"size="10" id="zip" value=${account.zip} ></td>
	</tr>
	<tr>
		<td>Country:</td>
		<td><input type="text" size="15" id="country" value=${account.country} ></td>
	</tr>
</table>

<h3>Profile Information</h3>

<table>
	<tr>
		<td>Language Preference:</td>
		<td>
		<select name="account.languagePreference" id="langPr">
			<option value="english" selected="selected">english</option>
			<option value="japanese">japanese</option>

		</select>
		</td>
	</tr>
	<tr>
		<td>Favourite Category:</td>
		<td>
				<select name="account.favouriteCategoryId" id="favocate">
					<option value="FISH">FISH</option>
					<option selected="selected" value="DOGS">DOGS</option>
					<option value="REPTILES">REPTILES</option>
					<option value="CATS">CATS</option>
					<option value="BIRDS">BIRDS</option>

				</select>
		</td>
	</tr>
	<tr>
		<td>Enable MyList</td>
		<td><input type="checkbox"  name="listOption"  value="${sessionScope.account.listOption}" ></td>
	</tr>
	<tr>
		<td>Enable MyBanner</td>
		<td><input type="checkbox" name="bannerOption" value="${sessionScope.account.bannerOption}"></td>
	</tr>

</table>
