package org.csu.db;

import java.sql.*;

public class DBUtils {
    private static final String URL = "jdbc:mysql://localhost:3306/jpetstore?useUnicode=true&autoReconnect=true&characterEncoding=UTF-8";
    private static final String DRIVAERCLASS = "com.mysql.jdbc.Driver";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "linlinyanyan299";

    public static Connection getConnection() throws Exception{
        Class.forName(DRIVAERCLASS);
        Connection connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        return connection;
    }

    public static void closeConnection(Connection connection)throws Exception{
        if(connection!=null){
            connection.close();
        }
    }
    public static void closeStatement(Statement statement)throws Exception{
        if(statement!=null){
            statement.close();
        }
    }
    public static void closePreparedStatement(PreparedStatement preparedStatement)throws Exception{
        if(preparedStatement!=null){
            preparedStatement.close();
        }
    }
    public static void closeResultSet(ResultSet resultSet)throws Exception{
        if(resultSet!=null){
            resultSet.close();
        }
    }


}