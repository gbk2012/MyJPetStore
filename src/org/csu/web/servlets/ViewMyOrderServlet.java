package org.csu.web.servlets;

import org.csu.domain.Order;
import org.csu.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewMyOrderServlet extends HttpServlet {
    private static final String VIEW_MY_ORDER="/WEB-INF/jsp/order/ViewOrder.jsp";

    private String username;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        username = req.getParameter("username");
        OrderService orderService = new OrderService();
        List <Order> orders = orderService.getOrdersByUsername(username);
        HttpSession session = req.getSession();
        session.setAttribute("order",orders);
        req.getRequestDispatcher(VIEW_MY_ORDER).forward(req,resp);
    }
}
