package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.service.AccountService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SaveInformationServlet extends HttpServlet {

    private String username = null;
    private String password = null;
    private String email = null;
    private String lastname = null;
    private String firstname = null;
    private String status = null;
    private String address1 = null;
    private String address2 = null;
    private String city = null;
    private String state = null;
    private String zip = null;
    private String country = null;
    private String phone = null;
    private String languPr = null;
    private  String favoricate = null;

    private String VIEW_SIGNONFORM="/WEB-INF/jsp/account/SignonForm.jsp";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        username = req.getParameter("username");
        password = req.getParameter("password");
        email = req.getParameter("email");
        lastname = req.getParameter("lastname");
        firstname = req.getParameter("firstname");
        status = req.getParameter("status");
        address1 = req.getParameter("address1");
        address2 = req.getParameter("address2");
        city = req.getParameter("city");
        state = req.getParameter("state");
        zip = req.getParameter("zip");
        country = req.getParameter("country");
        phone = req.getParameter("phone");
        languPr = req.getParameter("languagePreference");
        favoricate = req.getParameter("favouriteCategoryId");
        String listOption = req.getParameter("listOption");
        String bannerOption = req.getParameter("bannerOption");

        AccountService accountService = new AccountService();
        Account account = new Account();
        account.setUsername(username);
        account.setPassword(password);
        account.setPhone(phone);
        account.setCountry(country);
        account.setState(state);
        account.setCity(city);
        account.setAddress1(address1);
        account.setAddress2(address2);
        account.setStatus(status);
        account.setFirstName(firstname);
        account.setLastName(lastname);
        account.setLanguagePreference(languPr);
        account.setZip(zip);
        account.setFavouriteCategoryId(favoricate);
        account.setEmail(email);
        account.setListOption(Boolean.parseBoolean(listOption));
        account.setBannerOption(Boolean.parseBoolean(bannerOption));
        accountService.insertAccount(account);
        if(account != null){
            HttpServletRequest httpRequest= req;
            String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 注册新账号";
            logService.insertLogInfo(account.getUsername(), logInfo);
        }
        req.getRequestDispatcher(VIEW_SIGNONFORM).forward(req,resp);
    }
}
