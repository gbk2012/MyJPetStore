package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Cart;
import org.csu.domain.Item;
import org.csu.service.AccountService;
import org.csu.service.CartService;
import org.csu.service.CategortService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AddItemtoCartServlet extends HttpServlet {
    private static final String VIEW_CART = "/WEB-INF/jsp/cart/Cart.jsp";
    private static final String VIEW_SIGNONFORM = "/WEB-INF/jsp/account/SignonForm.jsp";
    private String workingItemId;
    private Cart cart;
    private int cartListsize;
    private CategortService categortService = new CategortService();
    private String username = null;
    Account account;
    private BigDecimal subtoatl;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        workingItemId = req.getParameter("workingItemId");
        username = req.getParameter("username");
        HttpSession session = req.getSession();
        account = (Account)session.getAttribute("account");
        Item item = categortService.getItem(workingItemId);
        AccountService accountService = new AccountService();
        username=req.getParameter("username");
        account = accountService.getAccount(username);
        CartService cartService = new CartService();
        if(account != null) {
            cart = (Cart) session.getAttribute("cart");
            HttpServletRequest httpRequest= req;
            String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 添加物品 " + item + " 到购物车";
            logService.insertLogInfo(account.getUsername(), logInfo);
            if (cart == null) {
                cart = new Cart();
            }
            if (cartService.isExit(username,workingItemId)>=0) {
                cart = cartService.getCart(username,workingItemId);
                String itemid=cart.itemid;
                try {
                    cart.setQuantity(cart.getQuantity()+1);
                    cart.setTotal(cart.getListprice().multiply(BigDecimal.valueOf(cart.quantity)));
                    cartService.Updatecart(username,workingItemId,cart);
                }catch (Exception e){
                    e.printStackTrace();
                }


            } else {
                categortService = new CategortService();
                boolean isInStock = categortService.isItemInStock(workingItemId);
                cart.addItem(item, isInStock);
                cart.setItemid(item.getItemId());
                cart.setProductid(item.getProductId());
                cart.setDescription(new CategortService().getProduct(item.getItemId()).getDescription());
                cart.setListprice(item.getListPrice());
                cart.setQuantity(1);
                cart.setStock(isInStock);
                cart.setTotal(cart.getListprice().multiply(BigDecimal.valueOf(cart.quantity)));
                cartService.insertintocart(cart,account.getUsername());
            }
            List<Cart> cartList = new ArrayList<>();
            cartList=cartService.getCartByusername(username);
            cartListsize = cartList.size();
            subtoatl=new BigDecimal(0);
            int i=0;
            while (i<cartListsize){
                subtoatl=subtoatl.add(cartList.get(i).getTotal());
                i++;
            }
            session.setAttribute("cartList",cartList);
            session.setAttribute("cartListsize",cartListsize);
            session.setAttribute("cart", cart);
            session.setAttribute("subTotal",subtoatl);
            req.getRequestDispatcher(VIEW_CART).forward(req, resp);
        }
        else {
            req.getRequestDispatcher(VIEW_SIGNONFORM).forward(req, resp);
        }
        }
    }

