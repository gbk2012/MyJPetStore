package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Cart;
import org.csu.domain.LineItem;
import org.csu.domain.Order;
import org.csu.service.CartService;
import org.csu.service.CategortService;
import org.csu.service.LogService;
import org.csu.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ViewOrderServlet extends HttpServlet {
    private static final String VIEWORDER = "/WEB-INF/jsp/order/ViewOrder.jsp";
    private static final String ERROR = "/WEB-INF/jsp/common/Error.jsp";
    private Order order;
    private OrderService orderService;
    private Cart cart;
    private String orderId=null;
    private String username=null;
    private BigDecimal subtoatl;
    private BigDecimal total=new BigDecimal(0.0);
    private List<Cart>cartList;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        orderId=request.getParameter("orderId");
        HttpSession session = request.getSession();
        Account account = (Account)session.getAttribute("account");
        username = request.getParameter("username");
        cartList = (List<Cart>)session.getAttribute("cartList");
        if(cartList!=null) {
            subtoatl=new BigDecimal(0);
            int k=0;
            while (k<cartList.size()){
                subtoatl=subtoatl.add(cartList.get(k).getTotal());
                k++;
            }
        }
        session.setAttribute("total",subtoatl);
        if(username != null){
            HttpServletRequest httpRequest= request;
            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 查看订单 " + order;
            logService.insertLogInfo(username, logInfo);
        }

        if(orderId!=null){
            orderService = new OrderService();
            order = orderService.getOrder(Integer.valueOf(orderId));
        }else {
            order = (Order) session.getAttribute("order");
            order.setTotalPrice(subtoatl);
            ArrayList<LineItem> lineItemIterator = new ArrayList<LineItem>();
            for(int i=0;i<cartList.size();i++){
                LineItem lineItem = new LineItem();
                lineItem.setUnitPrice(cartList.get(i).getListprice());
                lineItem.setQuantity(cartList.get(i).getQuantity());
                lineItem.setItemId(cartList.get(i).getItemid());
                lineItem.setItem(new CategortService().getItem(cartList.get(i).getItemid()));
                lineItem.setLineNumber(i+1);
               // lineItem.setOrderId(id);
                lineItemIterator.add(lineItem);
            }
            order.setLineItems(lineItemIterator);
        }
        cart = (Cart) session.getAttribute("cart");
        if (order != null) {
            orderService = new OrderService();
            orderService.insertOrder(order);
            session.setAttribute("order", order);
            CartService cartService = new CartService();
            //清空购物车
            cartService.deletecart_by_username(username);
            cart = null;
            session.setAttribute("cart", cart);

            session.setAttribute("message", "Thank you, your order has been submitted.");
            request.getRequestDispatcher(VIEWORDER).forward(request, response);
        } else {
            session.setAttribute("message", "An error occurred processing your order (order was null).");
            request.getRequestDispatcher(ERROR).forward(request, response);
        }
    }
}