package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.service.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ViewEditInformationFormServlet extends HttpServlet {
    private Account account = new Account();
    private String username = null;
    private static final String VIEW_EditAccountForm = "/WEB-INF/jsp/account/EditAccountForm.jsp";
    private static final String VIEW_SIGNONFORM="/WEB-INF/jsp/account/SignonForm.jsp";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        username = req.getParameter("username");
        if (username==null){
            req.getRequestDispatcher(VIEW_SIGNONFORM).forward(req,resp);
        }else {
        AccountService accountService = new AccountService();
        username = req.getParameter("username");
        account = accountService.getAccount(username);
        HttpSession session = req.getSession();
        session.setAttribute("account",account);
       req.getRequestDispatcher(VIEW_EditAccountForm).forward(req,resp);
    }
    }
}
