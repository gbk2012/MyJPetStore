package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Order;
import org.csu.service.LogService;
import org.csu.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewListOrderServlet extends HttpServlet {
    private static final String VIEW_LISTORDER="/WEB-INF/jsp/order/ListOrders.jsp";
    private String username;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OrderService orderService = new OrderService();
        username = req.getParameter("username");
        List<Order> orderList = orderService.getOrdersByUsername(username);
        HttpSession session =req.getSession();
        session.setAttribute("orderList",orderList);
        Account account = (Account)session.getAttribute("account");

        if(account != null){
            HttpServletRequest httpRequest= req;
            String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 查看订单 " + orderList;
            logService.insertLogInfo(account.getUsername(), logInfo);
        }

        req.getRequestDispatcher(VIEW_LISTORDER).forward(req,resp);
    }
}
