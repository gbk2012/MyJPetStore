package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Category;
import org.csu.domain.Item;
import org.csu.domain.Product;
import org.csu.service.CategortService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewProductServlet extends HttpServlet {
    private String pruductId;
    private String username;
    Category category = new Category();
    private static final String VIEW_PRODUCT = "/WEB-INF/jsp/catalog/Product.jsp";
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        pruductId=req.getParameter("productId");
        username=req.getParameter("username");
        HttpSession session = req.getSession();
        Account account = (Account)session.getAttribute("account");
        category = (Category) req.getAttribute("category");
        CategortService service = new CategortService();
        if(account!=null) {
            HttpServletRequest httpRequest = req;
            String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 查看产品 " + pruductId;
            logService.insertLogInfo(account.getUsername(), logInfo);
        }
        Product pruduct = service.getProductByProductID(pruductId);
        List<Item> itemlist= service.getItemListByProduct(pruductId);
        session.setAttribute("pruductId",pruductId);
        session.setAttribute("category",category);
        session.setAttribute("itemlist",itemlist);
        req.getRequestDispatcher(VIEW_PRODUCT).forward(req,resp);
    }
}
