package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Item;
import org.csu.domain.Product;
import org.csu.service.CategortService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewItemServlet extends HttpServlet {
    private String itemId;
    private static final String VIEW_ITEM="/WEB-INF/jsp/catalog/Item.jsp";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        itemId = req.getParameter("itemId");
        CategortService service = new CategortService();
        HttpSession session = req.getSession();
        Account account = (Account) session.getAttribute("account");
        Item item = service.getItem(itemId);
        List<Item> itemList = service.getItemListByProduct(itemId);
        if (account != null){
            HttpServletRequest httpRequest = req;
        String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

        LogService logService = new LogService();
        String logInfo = logService.logInfo(" ") + strBackUrl + " 查看具体商品 " + item;
        logService.insertLogInfo(account.getUsername(), logInfo);
    }
        session.setAttribute("item",item);
        session.setAttribute("itemList",itemList);
        Product pruduct = service.getProduct(itemId);
        session.setAttribute("product",pruduct);
        req.getRequestDispatcher(VIEW_ITEM).forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
