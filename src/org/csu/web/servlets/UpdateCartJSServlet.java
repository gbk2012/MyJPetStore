package org.csu.web.servlets;

import javafx.scene.control.Alert;
import org.csu.domain.Account;
import org.csu.domain.Cart;
import org.csu.domain.CartItem;
import org.csu.service.CartService;
import org.csu.service.CategortService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

public class UpdateCartJSServlet extends HttpServlet {

    private List<Cart> cartList;
    private CartService cartService= new CartService();
    private BigDecimal subtoatl;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //从对话中，获取购物车
        HttpSession session = request.getSession();
        Account account=(Account)session.getAttribute("account");
        String username = account.getUsername();
        cartList =cartService.getCartByusername(username);
        String[] strify = (String[])request.getParameterValues("quantity");
        String [] nq = new String[(strify[0].length()-1)];
        nq=strify[0].split(",");
//        for(int j=0,k=0;k<(strify[0].length()-1);j=j+2){
//            nq[k]=strify[0].split(",");
//            k++;
//        }
        int i=0;
        while (i<cartList.size()) {
            //产品数量增加
            Cart cart = (Cart) cartList.get(i);
            String itemId = cart.getItemid();
            try {
                cart.setQuantity(Integer.parseInt(String.valueOf(nq[i])));
                cart.setTotal(cart.getListprice().multiply(BigDecimal.valueOf(Integer.parseInt(String.valueOf(nq[i])))));
                cartService.Updatecart(username,itemId,cart);
                if (Integer.parseInt(String.valueOf(nq[i])) < 1) {
                    cartService.Delete_By_Username_and_Itemid(username,itemId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            i++;
        }

        subtoatl=new BigDecimal(0);
        int k=0;
        while (k<cartList.size()){
            subtoatl=subtoatl.add(cartList.get(k).getTotal());
            k++;
        }
        session.setAttribute("cartList", cartList);
        String quantityAll = "";
        int j=0;
        while (j<cartList.size()) {
            //产品数量增加
            Cart cart = (Cart) cartList.get(j);
            int quantity2 = cart.getQuantity();
            BigDecimal total = cart.getListprice().multiply(BigDecimal.valueOf(quantity2));
            quantityAll += quantity2 + "," + total + ",";
            j++;
        }
        quantityAll+=subtoatl;

        response.setContentType("text/xml");
        PrintWriter out = response.getWriter();
        out.write(quantityAll);
        session.setAttribute("subTotal",subtoatl);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
