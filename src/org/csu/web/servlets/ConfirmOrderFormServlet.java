package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Order;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ConfirmOrderFormServlet extends HttpServlet {
    private String shippingAddressRequired;
    Order order;
    private static final String CONFIRM_ORDER_FORM = "/WEB-INF/jsp/order/ConfirmOrder.jsp";
    private static final String SHIPPINGFORM = "/WEB-INF/jsp/order/ShippingForm.jsp";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        shippingAddressRequired = request.getParameter("shippingAddressRequired");
        order = new Order();

        HttpSession session = request.getSession();
        order = (Order) session.getAttribute("order");
        Account account = (Account) session.getAttribute("account");

        if (shippingAddressRequired == null) {
            if (account != null) {
                HttpServletRequest httpRequest = request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 确认生成订单 ";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }//对order赋值
            String cardType = request.getParameter("cardType");
            String creditCard = request.getParameter("creditCard");
            String expiryDate = request.getParameter("expiryDate");
            String billToFirstName = request.getParameter("billToFirstName");
            String billToLastName = request.getParameter("billToLastName");
            String billAddress1 = request.getParameter("billAddress1");
            String billAddress2 = request.getParameter("billAddress2");
            String billCity = request.getParameter("billCity");
            String billState = request.getParameter("billState");
            String billZip = request.getParameter("billZip");
            String billCountry = request.getParameter("billCountry");

            if (cardType != null) {
                order.setCardType(cardType);
            }
            if (creditCard != null) {
                order.setCreditCard(creditCard);
            }
            if (expiryDate != null) {
                order.setExpiryDate(expiryDate);
            }

            if (billToFirstName != null) {
                order.setBillToFirstName(billToFirstName);
            }
            if (billToLastName != null) {
                order.setBillToLastName(billToLastName);
            }
            if (billAddress1 != null) {
                order.setBillAddress1(billAddress1);
            }
            if (billAddress2 != null) {
                order.setBillAddress2(billAddress2);
            }
            if (billCity != null) {
                order.setBillCity(billCity);
            }
            if (billState != null) {
                order.setBillState(billState);
            }
            if (billZip != null) {
                order.setBillZip(billZip);
            }
            if (billCountry != null) {
                order.setBillCountry(billCountry);
            }
//            else {
//                order.setShipCountry(billCountry);
//            }
            session.setAttribute("order", order);
            request.getRequestDispatcher(CONFIRM_ORDER_FORM).forward(request, response);
        }
        else {
            shippingAddressRequired = null;

            if (account != null) {
                HttpServletRequest httpRequest = request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 更改收货地址";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }
            //对order赋值
            String cardType = request.getParameter("cardType");
            String creditCard = request.getParameter("creditCard");
            String expiryDate = request.getParameter("expiryDate");
            String billToFirstName = request.getParameter("billToFirstName");
            String billToLastName = request.getParameter("billToLastName");
            String billAddress1 = request.getParameter("billAddress1");
            String billAddress2 = request.getParameter("billAddress2");
            String billCity = request.getParameter("billCity");
            String billState = request.getParameter("billState");
            String billZip = request.getParameter("billZip");
            String billCountry = request.getParameter("billCountry");

            if (cardType != null) {
                order.setCardType(cardType);
            }
            if (creditCard != null) {
                order.setCreditCard(creditCard);
            }
            if (expiryDate != null) {
                order.setExpiryDate(expiryDate);
            }

            if (billToFirstName != null) {
                order.setBillToFirstName(billToFirstName);
            }
            if (billToLastName != null) {
                order.setBillToLastName(billToLastName);
            }
            if (billAddress1 != null) {
                order.setBillAddress1(billAddress1);
            }
            if (billAddress2 != null) {
                order.setBillAddress2(billAddress2);
            }
            if (billCity != null) {
                order.setBillCity(billCity);
            }
            if (billState != null) {
                order.setBillState(billState);
            }
            if (billZip != null) {
                order.setBillZip(billZip);
            }
            if (billCountry != null) {
                order.setBillCountry(billCountry);
            } else {
                order.setShipCountry(billCountry);
            }
            session.setAttribute("order", order);
            request.getRequestDispatcher(SHIPPINGFORM).forward(request, response);
        }
        }
    }
