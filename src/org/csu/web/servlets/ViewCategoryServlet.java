package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Category;
import org.csu.domain.Product;
import org.csu.service.CategortService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ViewCategoryServlet extends HttpServlet {
    private String category;
    private String username;
    private static final String VIEW_CATEGORY="/WEB-INF/jsp/catalog/Category.jsp";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        category=req.getParameter("categoryId");
        CategortService service = new CategortService();
        HttpSession session = req.getSession();
        Account account = new Account();
        account = (Account)session.getAttribute("account");
        Category category1 = service.getCategory(category);
        username = req.getParameter("username");
        List<Product> productList= service.getProductListByCategory(category);
        if(account != null) {
            HttpServletRequest httpRequest = req;
            String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 跳转到商品种类 " + category;
            logService.insertLogInfo(account.getUsername(), logInfo);
        }
        session.setAttribute("category",category1);
        session.setAttribute("productList",productList);
        req.getRequestDispatcher(VIEW_CATEGORY).forward(req,resp);
    }
}
