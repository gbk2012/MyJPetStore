package org.csu.web.servlets;

import org.csu.domain.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ViewSignonFormSevlet extends HttpServlet {
    private static final String VIEW_SIGNONFORM="/WEB-INF/jsp/account/SignonForm.jsp";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            req.setCharacterEncoding("UTF-8");
            req.getRequestDispatcher(VIEW_SIGNONFORM).forward(req, resp);
    }
}
