package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.service.AccountService;
import org.csu.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ViewIncludeAccountFieldsServlet extends HttpServlet {
    private static final String VIEW_INCLUDEACCOUNT="/WEB-INF/jsp/account/EditAccountForm.jsp";
    private static final String ERROR_MESSAGE="/WEB-INF/jsp/common/Error.jsp";
    private static final String VIEW_SIGNONFORM="/WEB-INF/jsp/account/SignonForm.jsp";
    private static final String MAIN = "/WEB-INF/jsp/catalog/Main.jsp";
    private String username;
    private String password;
    Account account =new Account();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AccountService accountService = new AccountService();
        username = req.getParameter("username");
        password = req.getParameter("password");
        HttpSession session = req.getSession();
        //获得输入的验证码值
        String value1=req.getParameter("vCode");
        /*获取图片的值*/
        String value2=(String)session.getAttribute("checkcode");
        Boolean isSame = false;
        /*对比两个值（字母不区分大小写）*/
        if(value2.equalsIgnoreCase(value1)){
            isSame = true;
        }
        if (accountService.getAccount(username,password)==null){
            session.setAttribute("message","Failed! Your uername or password is wrong");
            req.getRequestDispatcher(ERROR_MESSAGE).forward(req,resp);
        }else {
            if(isSame) {   account = accountService.getAccount(username,password);
                req.setAttribute("account",account);
                session.setAttribute("username",username);
                if(account != null){
                    HttpServletRequest httpRequest= req;
                    String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                            + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                    LogService logService = new LogService();
                    String logInfo = logService.logInfo(" ") + strBackUrl + " 用户登录";
                    logService.insertLogInfo(account.getUsername(), logInfo);
                }

                session.setAttribute("account",account);
                req.getRequestDispatcher(MAIN).forward(req,resp);
            }else {
                String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                        + req.getContextPath() + req.getServletPath() + "?" + (req.getQueryString());
                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 注册账号，验证码错误";
                logService.insertLogInfo(account.getUsername(), logInfo);
                //验证码输入错误后刷新界面，重新生成验证码
                req.getRequestDispatcher(VIEW_SIGNONFORM).forward(req, resp);
            }
    }
    }
    }

