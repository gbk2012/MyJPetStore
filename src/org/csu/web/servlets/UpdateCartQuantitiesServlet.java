package org.csu.web.servlets;

import org.csu.domain.Account;
import org.csu.domain.Cart;
import org.csu.service.CartService;
import org.csu.service.CategortService;
import org.csu.service.LogService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class UpdateCartQuantitiesServlet extends HttpServlet {

    private static final String VIEW_CART = "/WEB-INF/jsp/cart/Cart.jsp";

    private String workingItemId;
    private Cart cart;
    private List<Cart> cartList;
    private String username;
    private int cartListsize;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        workingItemId = request.getParameter("workingItemId");
        username = request.getParameter("username");
        CategortService catalogService = new CategortService();
        //从对话中，获取购物车
        HttpSession session = request.getSession();
        //cart = (Cart)session.getAttribute("cart");
        cartList = (List<Cart>)session.getAttribute("cartList");
        CartService cartService = new CartService();
        int i=0;
        while (i<cartList.size()){
            //产品数量增加
            cart = cartList.get(i);
            String itemid=cart.itemid;
            try {
                int quantity = Integer.parseInt((String) request.getParameter(itemid));
                cart.setQuantity(quantity);
                cart.setTotal(cart.getListprice().multiply(BigDecimal.valueOf(cart.quantity)));
                cartService.Updatecart(username,itemid,cart);
                if (quantity < 1) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            i++;
        }
        session.setAttribute("cartList",cartList);
        Account account = (Account)session.getAttribute("account");
        if(account != null){
            HttpServletRequest httpRequest= request;
            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 更新购物车商品数量";
            logService.insertLogInfo(account.getUsername(), logInfo);
        }
        cartList = (List<Cart>)session.getAttribute("cartList");
        cartListsize = cartList.size();
        session.setAttribute("cartListsize",cartListsize);
        session.setAttribute("cartList",cartList);
        request.getRequestDispatcher(VIEW_CART).forward(request, response);
    }
}
