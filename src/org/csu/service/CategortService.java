package org.csu.service;

import org.csu.domain.Category;
import org.csu.domain.Item;
import org.csu.domain.Product;
import org.csu.persistence.CategoryDAO;
import org.csu.persistence.IMPL.CategortDAOImpl;
import org.csu.persistence.IMPL.ItemDAOImpl;
import org.csu.persistence.ItemDAO;
import org.csu.persistence.ProductDAO;
import org.csu.persistence.IMPL.ProductDAOImpl;

import java.util.List;

public class CategortService {
    private CategoryDAO categoryDAO;
    private ProductDAO productDAO;
    private ItemDAO itemDAO;

    public CategortService() {
        categoryDAO = new CategortDAOImpl();
        productDAO = new ProductDAOImpl();
        itemDAO = new ItemDAOImpl();
    }
    public List<Category> getCategoryList() {
        return categoryDAO.getCategoryList();
    }

    public Category getCategory(String categoryId) {
        return categoryDAO.getCategory(categoryId);
    }

    public Product getProduct(String itemId) {
        return productDAO.getProduct(productDAO.getProductId(itemId));
    }
    public Product getProductByname(String name){
        return productDAO.getProductByname(name);
    }
    public Product getProductByProductID(String productId){return productDAO.getProduct(productId);}

    public List<Product> getProductListByCategory(String categoryId) {
        return productDAO.getProductListByCategory(categoryId);
    }

    // TODO enable using more than one keyword
    public List<Product> searchProductList(String keyword) {
        return productDAO.searchProductList("%" + keyword.toLowerCase() + "%");
    }

    public List<Item> getItemListByProduct(String productId) {
        return itemDAO.getItemListByProduct(productId);
    }

    public Item getItem(String itemId) {
        return itemDAO.getItem(itemId);
    }

    public boolean isItemInStock(String itemId) {
        return itemDAO.getInventoryQuantity(itemId) > 0;
    }

}
