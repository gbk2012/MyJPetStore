package org.csu.service;

import org.csu.domain.Account;
import org.csu.persistence.AccountDAO;
import org.csu.persistence.IMPL.AccountDAOImple;

public class AccountService {

  private AccountDAO accountMapper;

  public Account getAccount(String username) {
    accountMapper = new AccountDAOImple();
    return accountMapper.getAccountByUsername(username);
  }

  public Account getAccount(String username, String password) {
    Account account = new Account();
    account.setUsername(username);
    account.setPassword(password);
    accountMapper = new AccountDAOImple();
    return accountMapper.getAccountByUsernameAndPassword(account);
  }

  public void insertAccount(Account account) {
    accountMapper = new AccountDAOImple();
    accountMapper.insertAccount(account);
    accountMapper.insertProfile(account);
    accountMapper.insertSignon(account);
  }

  public void updateAccount(Account account) {
    accountMapper = new AccountDAOImple();
    accountMapper.updateAccount(account);
    accountMapper.updateProfile(account);

    if (account.getPassword() != null && account.getPassword().length() > 0) {
      accountMapper = new AccountDAOImple();
      accountMapper.updateSignon(account);
    }
  }

}
