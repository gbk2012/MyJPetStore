package org.csu.service;

import org.csu.domain.Cart;
import org.csu.persistence.CartDAO;
import org.csu.persistence.IMPL.CartDAOImpl;

import java.util.List;

public class CartService {
    CartDAO cartDAO;

    public void insertintocart(Cart cart ,String usrename){
        CartDAO cartDAO = new CartDAOImpl();
        cartDAO.insertCart(cart,usrename);
    }
    public List<Cart> getCartByusername(String username){
        CartDAO cartDAO = new CartDAOImpl();
        return cartDAO.getCartByusername(username);
    }
    public void deletecart_by_username(String username){
        CartDAO cartDAO = new CartDAOImpl();
        cartDAO.deletecart(username);
    }

    public void Updatecart(String username,String itemid,Cart cart){
        CartDAO cartDAO = new CartDAOImpl();
        cartDAO.Updatecart(username,itemid,cart);
    }
    public void Delete_By_Username_and_Itemid(String username,String itemid){
        CartDAO cartDAO = new CartDAOImpl();
        cartDAO.deletecartByusername_and_itemid(username,itemid);
    }
    public int isExit(String username,String itemid){
        CartDAO cartDAO = new CartDAOImpl();
        return cartDAO.isExist(username,itemid);
    }
    public Cart getCart(String username,String Itemid){
        CartDAO cartDAO = new CartDAOImpl();
        return cartDAO.getCartByusername(username,Itemid);
    }
}
