package org.csu.persistence;

public interface LogDAO {
    void insertLog(String username, String logInfo);
}
