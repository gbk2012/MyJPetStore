package org.csu.persistence;

import org.csu.domain.Cart;

import java.util.List;

public interface CartDAO {
    void insertCart(Cart cart,String username);

    List<Cart> getCartByusername(String username);

    Cart getCartByusername(String username,String itemid);

    void deletecart(String username);

    void Updatecart(String username,String itemId, Cart cart);

    void deletecartByusername_and_itemid(String username,String itemid);

    int isExist(String username,String itemid);
}
