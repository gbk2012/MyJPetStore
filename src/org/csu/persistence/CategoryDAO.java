package org.csu.persistence;

import org.csu.domain.Category;

import java.util.List;

public interface CategoryDAO {

    //查询所有的商品
    List<Category> getCategoryList();

    //通过id查询某一商品
    Category getCategory(String categoryId);

}
