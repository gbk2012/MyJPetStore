package org.csu.persistence.IMPL;

import org.csu.db.DBUtils;
import org.csu.domain.Cart;
import org.csu.persistence.CartDAO;
import sun.dc.pr.PRError;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CartDAOImpl implements CartDAO {
    private static final String INSERT_CART_BY_CART_AND_USERNAME="INSERT INTO cart VALUES(?,?,?,?,?,?,?,?)";
    private static final String GET_CART_BY_USERNMAE ="SELECT * FROM cart WHERE username=?";
    private static final String DELETE_CART_BY_USERNAME = "DELETE FROM cart WHERE username=?";
    private static final String UPDATE_CART_BY_USERNAME_AND_ITEMID="UPDATE cart SET quantity=?,totalcost=? WHERE username=? AND itemid=?";
    private static final String DELETE_BY_USERNAME_AND_ITEMID = "DELETE FROM cart WHERE username=? AND itemid=?";
    private static final String ISEXIT_BY_USERNAME_ITEMID = "SELECT * FROM cart WHERE username=? AND itemid=?";
    private static final String GET_CART_BY_USERNAME_AND_ITEMID="SELECT * FROM cart WHERE username=? AND itemid=?";
    @Override
    public void insertCart(Cart cart, String username) {
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CART_BY_CART_AND_USERNAME);
            preparedStatement.setString(1, username);
                preparedStatement.setString(2,cart.getItemid());
                preparedStatement.setString(3,cart.getProductid());
                preparedStatement.setString(4,cart.getDescription());
                preparedStatement.setInt(5,cart.getQuantity());
                preparedStatement.setBoolean(6,cart.getStock());
                preparedStatement.setBigDecimal(7,cart.getListprice());
                preparedStatement.setBigDecimal(8,cart.getTotal());
                preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Cart> getCartByusername(String username) {
        List<Cart> cartList = new ArrayList<>();
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_CART_BY_USERNMAE);
            preparedStatement.setString(1,username);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Cart cart = new Cart();
                cart.setUsername(resultSet.getString(1));
                cart.setItemid(resultSet.getString(2));
                cart.setProductid(resultSet.getString(3));
                cart.setDescription(resultSet.getString(4));
                cart.setQuantity(resultSet.getInt(5));
                cart.setStock(resultSet.getBoolean(6));
                cart.setListprice(resultSet.getBigDecimal(7));
                cart.setTotal(resultSet.getBigDecimal(8));
                cartList.add(cart);
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return cartList;
    }

    @Override
    public void deletecart(String username) {
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CART_BY_USERNAME);
            preparedStatement.setString(1, username);
                preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Updatecart(String username, String itemId , Cart cart) {
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CART_BY_USERNAME_AND_ITEMID);
            preparedStatement.setInt(1, cart.getQuantity());
            preparedStatement.setBigDecimal(2,cart.getTotal());
            preparedStatement.setString(3,username);
            preparedStatement.setString(4,itemId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletecartByusername_and_itemid(String username, String itemid) {
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_USERNAME_AND_ITEMID);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2,itemid);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int isExist(String username, String itemid) {
        int result=-1;
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ISEXIT_BY_USERNAME_ITEMID);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2,itemid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                result = 1;
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Cart getCartByusername(String username, String itemid) {
        Cart cart = null;
        try {
            Connection connection = DBUtils.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(GET_CART_BY_USERNAME_AND_ITEMID);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2,itemid);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                cart = new Cart();
                cart.setUsername(resultSet.getString(1));
                cart.setItemid(resultSet.getString(2));
                cart.setProductid(resultSet.getString(3));
                cart.setDescription(resultSet.getString(4));
                cart.setQuantity(resultSet.getInt(5));
                cart.setStock(resultSet.getBoolean(6));
                cart.setListprice(resultSet.getBigDecimal(7));
                cart.setTotal(resultSet.getBigDecimal(8));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return cart;
    }
}
