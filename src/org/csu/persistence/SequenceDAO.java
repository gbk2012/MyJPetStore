package org.csu.persistence;

import org.csu.domain.Sequence;

public interface SequenceDAO  {
    Sequence getSequence(Sequence sequence);
    void updateSequence(Sequence sequence);
}
