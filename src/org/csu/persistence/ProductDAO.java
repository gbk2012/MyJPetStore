package org.csu.persistence;

import org.csu.domain.Product;

import java.util.List;

public interface ProductDAO {
    List<Product> getProductListByCategory(String categoryId);

    Product getProduct(String productId);
    String getProductId(String itemId);
    Product getProductByname(String name);

    List<Product> searchProductList(String keywords);


}
