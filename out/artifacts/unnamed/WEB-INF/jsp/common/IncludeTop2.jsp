
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<meta charset="UTF-8">
<head>
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css" media="screen" />
    <link rel="StyleSheet" href="css/searchProduct.css" type="text/css" media="screen" />
    <link rel="StyleSheet" href="css/cartChange.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/mouseEventInform.css" type="text/css" media="screen" />

    <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type" />
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css"
          media="screen" />

    <meta name="generator"
          content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252"
          http-equiv="Content-Type" />
    <meta http-equiv="Cache-Control" content="max-age=0" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
</head>

<body>

<div id="Header">

    <div id="Logo">
        <div id="LogoContent">
            <a href="main"><img src="images/logo-topbar.gif" /></a>
        </div>
    </div>

    <div id="Menu">
        <div id="MenuContent">
            <a href="Viewcart?account=${sessionScope.account}&username=${sessionScope.username}"><img align="middle" name="img_cart" src="images/cart.gif" /></a> <img align="middle" src="images/separator.gif" />
            <c:if test="${sessionScope.username == null}">
                <a href="ViewSignonForm">Sign In</a>
            </c:if>
            <c:if test="${sessionScope.username != null}">
                <a href="SignOut">Sign Out</a>
                <!---signOff-->
            </c:if>

            <!--分隔符-->
            <c:if test="${sessionScope.username != null}">
                <img align="middle" src="images/separator.gif" />
                <a href="ViewEditInformationForm?username=${sessionScope.username}">My Account</a>
            </c:if><img align="middle" src="images/separator.gif" />

            <c:if test="${sessionScope.username != null}">
                <a href="ViewListOrder?username=${account.username}">My Orders</a>
            </c:if>
            <c:if test="${sessionScope.username != null}">
                <img align="middle" src="images/separator.gif" />
            </c:if>
            <a href="ViewJPetStore">?</a>
        </div>
    </div>

    <div id="Search">
        <div id="SearchContent">
            <form action="search" method="post">
                <input type="text" id="keyword" name="keyword" size="14"/>
                <div class="auto hidden" id="auto">
                    <div class="auto_out">1</div>
                    <div class="auto_out">2</div>
                </div>
                <input type="submit" name="search" value="Search" />
                <script src="js/searchProduct.js"></script>
            </form>
        </div>
    </div>

    <div id="QuickLinks">
        <a href="ViewCategory?categoryId=FISH&username=${sessionScope.username}"><img
                src="images/sm_fish.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?categoryId=DOGS&username=${sessionScope.username}"><img
                src="images/sm_dogs.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?categoryId=REPTILES&username=${sessionScope.username}"><img
                src="images/sm_reptiles.gif" />
        </a> <img
            src="images/separator.gif" />
        <a href="ViewCategory?categoryId=CATS&username=${sessionScope.username}"><img
                src="images/sm_cats.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?categoryId=BIRDS&username=${sessionScope.username}"><img
                src="images/sm_birds.gif" />
        </a>
    </div>
</div>
