<%@ include file="../common/IncludeTop2.jsp"%>
<style type="text/css">
	* {
		box-sizing: border-box;
	}
	.container1 {
		margin: 0;
		padding: 0;
		font: 16px/20px microsft yahei;
	}
	.wrap1 {
		width: 99%;
		height: 600px;
		padding: 50px 0;
		position: sticky;
		top:30%;
		opacity: 0.8;
		background: linear-gradient(to bottom right, #f1f8f2, #a7dc79);
	}
	.container1 {
		width: 70%;
		margin: 0 auto;
	}
	.container1 h1 {
		text-align: center;
		color: #FFFFFF;
		font-weight: 500;
	}
	.container1 input {
		width: 280px;
		display: block;
		height: 35px;
		border: 0;
		outline: 0;
		padding: 6px 10px;
		line-height: 2px;
		margin: 16px auto;
		-webkit-transition: all 0s ease-in 0.1ms;
		-moz-transition: all 0s ease-in 0.1ms;
		transition: all 0s ease-in 0.1ms;
	}
	.container1 input[type="text"] , .container1 input[type="password"]  {
		box-shadow: 5px 5px rgba(0,0,0,.3);
		text-align: center;
		background-color: #FFFFFF;
		border-width: 1ex;
		border-color: #65a765;
		font-size: 16px;
		color: #50a3a2;
		margin-left: 3px;
	}
	.container1 input[type='submit'] {
		box-shadow: 5px 5px rgba(0,0,0,.3);
		margin-top: 30px;
		font-size: 20px;
		letter-spacing: 2px;
		color: #666666;
		width: 120px;
		height: 50px;
		background-color: #FFFFFF;
		border-radius: 50%;
	}
	.container1 #Catalog1 form {
		padding-left: 20px;
	}
	.container1 input:focus {
		width: 400px;
	}
	.container1 #Catalog1 .mini {
		width: 0;
		height: 0;
		line-height: 0;
		font-size: 0;
		border: 20px solid transparent;
		border-left-color: #65a765;
		float: left;
	}
	.container1 #Catalog1 .mini1 {
		width: 0;
		height: 0;
		line-height: 0;
		font-size: 0;
		border:18px solid transparent;
		border-left-color: #65a765;
		float: left;
		margin-left: 190px;
	}
	.container1 #Catalog1 .yuandian {
		width: 20px;
		height: 20px;
		border-radius: 50%;
		background-color: #65a765;
		border-color: #65a765;
		float: left;
	}
	.container1 input[type='submit']:hover {
		cursor: pointer;
		width: 200px;
		height: 50px;
	}
	.container1 #Catalog1 h6 {
		line-height: 2ex;
		font: bold 20px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		color: #4f7258;
		display: inline-block;
		font-weight:normal;
		padding-top: 20px;
		padding-left: 60px;
	}

	.wrap1 ul {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: -10;
	}
	.wrap1 ul li {
		list-style-type: none;
		display: block;
		position: absolute;
		bottom: -120px;
		width: 15px;
		height: 15px;
		z-index: -8;
		background-color:rgba(255, 255, 255, 0.15);
		animotion: square 25s infinite;
		-webkit-animation: square 25s infinite;
	}
	.wrap1 ul li:nth-child(1) {
		background-color: #1a521a;
		left: 2%;
		animation-duration: 10s;
		-moz-animation-duration: 10s;
		-o-animation-duration: 10s;
		-webkit-animation-duration: 10s;
	}
	.wrap1 ul li:nth-child(2) {
		width: 40px;
		height: 40px;
		background-color: #abd7ab;
		left: 10%;
		animation-duration: 15s;
		-moz-animation-duration: 15s;
		-o-animation-duration: 15s;
		-webkit-animation-duration: 15s;
	}
	.wrap1 ul li:nth-child(3) {
		background-color: rgba(255,255,255,0.5);
		left: 18%;
		width: 25px;
		height: 25px;
		animation-duration: 12s;
		-moz-animation-duration: 12s;
		-o-animation-duration: 12s;
		-webkit-animation-duration: 12s;
	}
	.wrap1 ul li:nth-child(4) {
		background-color: #65a765;
		width: 50px;
		height: 50px;
		left: 25%;
		-webkit-animation-delay: 3s;
		-moz-animation-delay: 3s;
		-o-animation-delay: 3s;
		animation-delay: 3s;
		animation-duration: 12s;
		-moz-animation-duration: 12s;
		-o-animation-duration: 12s;
		-webkit-animation-duration: 12s;
	}
	.wrap1 ul li:nth-child(5) {
		background-color: rgba(255,255,255,0.5);
		width: 60px;
		height: 60px;
		left: 35%;
		animation-duration: 10s;
		-moz-animation-duration: 10s;
		-o-animation-duration: 10s;
		-webkit-animation-duration: 10s;
	}
	.wrap1 ul li:nth-child(6) {
		background-color: #92c185;
		width: 75px;
		height: 75px;
		left: 45%;
		-webkit-animation-delay: 7s;
		-moz-animation-delay: 7s;
		-o-animation-delay: 7s;
		animation-delay: 7s;
	}
	.wrap1 ul li:nth-child(7) {
		background-color: #65a765;
		left: 55%;
		animation-duration: 8s;
		-moz-animation-duration: 8s;
		-o-animation-duration: 8s;
		-webkit-animation-duration: 8s;
	}
	.wrap1 ul li:nth-child(8) {
		background-color: rgba(255,255,255,0.5);
		width: 90px;
		height: 90px;
		left: 62%;
		-webkit-animation-delay: 4s;
		-moz-animation-delay: 4s;
		-o-animation-delay: 4s;
		animation-delay: 4s;
	}
	.wrap1 ul li:nth-child(9) {
		width: 100px;
		height: 100px;
		left: 75%;
		animation-duration: 20s;
		-moz-animation-duration: 20s;
		-o-animation-duration: 20s;
		background-color: #6ebd6e;
		-webkit-animation-duration: 20s;
	}
	.wrap1 ul li:nth-child(10) {
		background-color: rgba(255,255,255,0.5);
		width: 120px;
		height: 120px;
		left: 88%;
		-webkit-animation-delay: 6s;
		-moz-animation-delay: 6s;
		-o-animation-delay: 6s;
		animation-delay: 6s;
		animation-duration: 30s;
		-moz-animation-duration: 30s;
		-o-animation-duration: 30s;
		-webkit-animation-duration: 30s;
	}

	@keyframes square {
		0%  {
			-webkit-transform: translateY(0);
			transform: translateY(0)
		}
		100% {
			bottom: 400px;
			transform: rotate(600deg);
			-webit-transform: rotate(600deg);
			-webkit-transform: translateY(-500);
			transform: translateY(-500)
		}
	}
	@-webkit-keyframes square {
		0%  {
			-webkit-transform: translateY(0);
			transform: translateY(0)
		}
		100% {
			bottom: 400px;
			transform: rotate(600deg);
			-webit-transform: rotate(600deg);
			-webkit-transform: translateY(-500);
			transform: translateY(-500)
		}
	}
</style>
<div class="wrap1">
	<div class="container1">
		<div id="Catalog1">
			<form action="ViewIncludeAccountFields" method="post">
				<div class="yuandian"></div><div class="yuandian"></div><div class="yuandian"></div><h2>Please enter your username and password</h2>
				<p><div class="mini1"></div><input type="text"  name="username" id="username" onblur="usernameIsExist()" placeholder="username" />
				<div id="usernameMsg" style="text-align: center"></div><script type="text/javascript" src="${pageContext.request.contextPath }/js/login.js"></script>
				<br />
				<div class="mini1"></div><input type="password"  name="password" placeholder="password" /></p>
				<div class="mini1"></div><input type="text" name="vCode" placeholder="VerificationCode" size="5" maxlength="4"/>
				<div style="text-align: center"><a href="main"><img border="0" src="VerificationCode" name="checkcode"></a></div>
				<input type="submit"  name="signon" value="Login" />
			</form>
			<div class="mini"></div><div class="mini"></div><div class="mini"></div><h6>Need a user name and password?&nbsp;<a href="ViewNewAccount">Register Now</a>&nbsp;!</h6>
		</div>
	</div>
	<ul>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>

</body>
</html>

