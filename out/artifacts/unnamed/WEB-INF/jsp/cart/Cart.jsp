<%@ include file="../common/IncludeTop.jsp"%>

<div id="BackLink"><a href="main">
	Return to Main Menu</a></div>

<div id="Catalog">

<div id="Cart">

<h2>Shopping Cart</h2>
<form action="UpdateCartQuantities?username=${sessionScope.username}" method="post">
	<style type="text/css">

		table {
			width: 700px;
			padding: 0;
			margin: 0;
		}

		caption {
			padding: 0 0 5px 0;
			width: 700px;
			font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			text-align: right;
		}

		th {
			font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			color: #4f6b72;
			border-right: 1px solid #C1DAD7;
			border-bottom: 1px solid #C1DAD7;
			border-top: 1px solid #C1DAD7;
			letter-spacing: 2px;
			text-transform: uppercase;
			text-align: left;
			padding: 6px 6px 6px 12px;
			background: #d5eaca no-repeat;
		}

		th.nobg {
			border-top: 0;
			border-left: 0;
			border-right: 1px solid #C1DAD7;
			background: none;
		}

		td {
			border-right: 1px solid #C1DAD7;
			border-bottom: 1px solid #C1DAD7;
			background: #fff;
			font-size:16px;
			padding: 6px 6px 6px 12px;
			color: #4f6b72;
		}


		td.alt {
			background: #F5FAFA;
			color: #797268;
		}

		th.spec {
			border-left: 1px solid #C1DAD7;
			border-top: 0;
			background: #fff no-repeat;
			font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		}

		th.specalt {
			border-left: 1px solid #C1DAD7;
			border-top: 0;
			background: #f5fafa no-repeat;
			font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
			color: #797268;
		}
	</style>
	<table id="tab">
		<tr>
			<th><b>Item ID</b></th>
			<th><b>Product ID</b></th>
			<th><b>Description</b></th>
			<th><b>In Stock?</b></th>
			<th><b>Quantity</b></th>
			<th><b>List Price</b></th>
			<th><b>Total Cost</b></th>
			<th>&nbsp;</th>
		</tr>

		<c:if test="${sessionScope.cartListsize == 0}">
			<tr>
				<td colspan="8"><b>Your cart is empty.</b></td>
			</tr>
		</c:if>

		<c:forEach var="cart" items="${sessionScope.cartList}">
			<tr>
				<td>
				<a href="ViewItem?itemId=${cart.itemid}">${cart.itemid}</a>
				</td>
				<td>${cart.productid}</td>
				<td>${cart.description}</td>
				<td>${cart.stock}</td>
				<td>
					<input type="text" id="quantity" class="im" onblur="updateCart();" name="${cart.itemid}"  value="${cart.quantity}">
					<script type="text/javascript" src="${pageContext.request.contextPath }/js/updateCart.js"></script>
				</td>
				<td><fmt:formatNumber value="${cart.listprice}"
					pattern="$#,##0.00" /></td>
				<td>
					<label id="total">${cart.total}</label>
					<!--<fmt:formatNumber value="${cart.total}"
					pattern="$#,##0.00" />-->
				</td>
				<td>
					<a href="RemoveItem?itemid=${cart.itemid}&username=${sessionScope.username}" class="Button"> Remove </a>
            </td>
			</tr>
			<script src="js/cartChange.js"></script>

		</c:forEach>
		<tr>
		<tr id="lastTR">
			<td colspan="7" id="lastTD">
				Sub Total:<label id="subtotal">${sessionScope.subTotal}</label>
				<!--<fmt:formatNumber value="${sessionScope.cart.subTotal}" pattern="$#,##0.00" />-->
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>

			&nbsp;
		</tr>
	</table>

</form>
	<c:if test="${sessionScope.cartListsize > 0}">
	<a href="NewOrderForm?cartItem=${sessionScope.cart.cartItems}&account=${sessionScope.account}&username=${sessionScope.username}" class="Button">Proceed to Checkout</a>
</c:if></div>


<div id="Separator">&nbsp;</div>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>