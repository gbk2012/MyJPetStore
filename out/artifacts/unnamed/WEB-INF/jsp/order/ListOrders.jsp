<%@ include file="../common/IncludeTop.jsp"%>

<h2 align="center">My Orders</h2>
<style type="text/css">

	table {
		width: 700px;
		padding: 0;
		margin: 0 auto;
	}

	caption {
		padding: 0 0 5px 0;
		width: 700px;
		font: italic 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		text-align: right;
	}

	th {
		font: bold 16px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		color: #4f6b72;
		border-right: 1px solid #C1DAD7;
		border-bottom: 1px solid #C1DAD7;
		border-top: 1px solid #C1DAD7;
		letter-spacing: 2px;
		text-transform: uppercase;
		text-align: left;
		padding: 6px 6px 6px 12px;
		background: #d5eaca no-repeat;
	}

	th.nobg {
		border-top: 0;
		border-left: 0;
		border-right: 1px solid #C1DAD7;
		background: none;
	}

	td {
		border-right: 1px solid #C1DAD7;
		border-bottom: 1px solid #C1DAD7;
		background: #fff;
		font-size:16px;
		padding: 6px 6px 6px 12px;
		color: #4f6b72;
	}


	td.alt {
		background: #F5FAFA;
		color: #797268;
	}

	th.spec {
		border-left: 1px solid #C1DAD7;
		border-top: 0;
		background: #fff no-repeat;
		font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
	}

	th.specalt {
		border-left: 1px solid #C1DAD7;
		border-top: 0;
		background: #f5fafa no-repeat;
		font: bold 10px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
		color: #797268;
	}
</style>
<table>
	<tr>
		<th>Order ID</th>
		<th>Date</th>
		<th>Total Price</th>
	</tr>

	<c:forEach var="order" items="${sessionScope.orderList}">
		<tr>
			<td>
			<a href="ViewOrder?orderId=${order.orderId}" name="orderId" value="${order.orderId}">${order.orderId}</a></td>
			<td><fmt:formatDate value="${order.orderDate}"
				pattern="yyyy/MM/dd hh:mm:ss" /></td>
			<td><fmt:formatNumber value="${order.totalPrice}"
				pattern="$#,##0.00" /></td>
		</tr>
	</c:forEach>
</table>

<%@ include file="../common/IncludeBottom.jsp"%>


