
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css" media="screen" />
    <link rel="StyleSheet" href="css/searchProduct.css" type="text/css" media="screen" />
    <link rel="StyleSheet" href="css/cartChange.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/mouseEventInform.css" type="text/css" media="screen" />

    <meta name="generator" content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252" http-equiv="Content-Type" />
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css"
          media="screen" />

    <meta name="generator"
          content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org" />
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252"
          http-equiv="Content-Type" />
    <meta http-equiv="Cache-Control" content="max-age=0" />
    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="Pragma" content="no-cache" />
    <style>
        *{
            margin:0;
            padding:0;
        }

        li{
            list-style: none;
        }
        .box{
            width:200px;
            height:200px;
            position: relative;
            margin:300px auto;
            -webkit-transform-style:preserve-3d;
            -webkit-transform:rotateX(13deg);
            -webkit-animation:move 5s linear infinite;
        }
        .minbox{
            width:200px;
            height:200px;
            position: absolute;
            left:50px;
            top:50px;
            -webkit-transform-style:preserve-3d;
        }
        .minbox li{
            width:100px;
            height:100px;
            position: absolute;
            left:0;
            top:0;
        }
        .minbox li:nth-child(1){
            background: url(images/One.jpg) no-repeat 0 0;
            -webkit-transform:translateZ(50px);
        }
        .minbox li:nth-child(2){
            background: url(images/Two.jpg) no-repeat 0 0;
            -webkit-transform:rotateX(180deg) translateZ(50px);
        }
        .minbox li:nth-child(3){
            background: url(images/Three.jpg) no-repeat 0 0;
            -webkit-transform:rotateX(-90deg) translateZ(50px);
        }
        .minbox li:nth-child(4){
            background: url(images/Four.jpg) no-repeat 0 0;
            -webkit-transform:rotateX(90deg) translateZ(50px);
        }
        .minbox li:nth-child(5){
            background: url(images/Five.jpg) no-repeat 0 0;
            -webkit-transform:rotateY(-90deg) translateZ(50px);
        }
        .minbox li:nth-child(6){
            background: url(images/Six.jpg) no-repeat 0 0;
            -webkit-transform:rotateY(90deg) translateZ(50px);
        }
        .maxbox{
            width: 200px;
            height: 200px;
            position: absolute;
            left: 0;
            top: 0;
            -webkit-transform-style: preserve-3d;
        }
        .maxbox li{
            width: 200px;
            height: 200px;
            background: #fff;
            border:1px solid #ccc;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0.2;
            -webkit-transition:all 1s ease;
        }
        .maxbox li:nth-child(1){
            -webkit-transform:translateZ(100px);
        }
        .maxbox li:nth-child(2){
            -webkit-transform:rotateX(180deg) translateZ(100px);
        }
        .maxbox li:nth-child(3){
            -webkit-transform:rotateX(-90deg) translateZ(100px);
        }
        .maxbox li:nth-child(4){
            -webkit-transform:rotateX(90deg) translateZ(100px);
        }
        .maxbox li:nth-child(5){
            -webkit-transform:rotateY(-90deg) translateZ(100px);
        }
        .maxbox li:nth-child(6){
            -webkit-transform:rotateY(90deg) translateZ(100px);
        }
        .box:hover ol li:nth-child(1){
            -webkit-transform:translateZ(300px);
        }
        .box:hover ol li:nth-child(2){
            -webkit-transform:rotateX(180deg) translateZ(300px);
        }
        .box:hover ol li:nth-child(3){
            -webkit-transform:rotateX(-90deg) translateZ(300px);
        }
        .box:hover ol li:nth-child(4){
            -webkit-transform:rotateX(90deg) translateZ(300px);
        }
        .box:hover ol li:nth-child(5){
            -webkit-transform:rotateY(-90deg) translateZ(300px);
        }
        .box:hover ol li:nth-child(6){
            -webkit-transform:rotateY(90deg) translateZ(300px);
        }
        @keyframes move{
            0%{
                -webkit-transform: rotateX(13deg) rotateY(0deg);
            }
            100%{
                -webkit-transform:rotateX(13deg) rotateY(360deg);
            }
        }
    </style>
</head>

<body>

<div id="Header">

    <div id="Logo">
        <div id="LogoContent">
            <a href="main"><img src="images/logo-topbar.gif" /></a>
        </div>
    </div>

    <div id="Menu">
        <div id="MenuContent">
            <a href="Viewcart?account=${sessionScope.account}&username=${sessionScope.username}"><img align="middle" name="img_cart" src="images/cart.gif" /></a> <img align="middle" src="images/separator.gif" />
            <c:if test="${sessionScope.username == null}">
                <a href="ViewSignonForm">Sign In</a>
            </c:if>
            <c:if test="${sessionScope.username != null}">
                <a href="SignOut">Sign Out</a>
                <!---signOff-->
            </c:if>

            <!--分隔符-->
            <c:if test="${sessionScope.username != null}">
                <img align="middle" src="images/separator.gif" />
                <a href="ViewEditInformationForm?username=${sessionScope.username}">My Account</a>
            </c:if><img align="middle" src="images/separator.gif" />
            <a href="ViewJPetStore">?</a>
        </div>
    </div>

    <div id="Search">
        <div id="SearchContent">
            <form action="search" method="post">
                <input type="text" id="keyword" name="keyword" size="14"/>
                <div class="auto hidden" id="auto">
                    <div class="auto_out">1</div>
                    <div class="auto_out">2</div>
                </div>
                <input type="submit" name="searchProducts" value="Search" />
                <script src="js/searchProduct.js"></script>
            </form>
        </div>
    </div>

    <div id="QuickLinks">
        <a href="ViewCategory?category=FISH&username=${sessionScope.username}"><img
                src="images/sm_fish.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?category=DOGS&username=${sessionScope.username}"><img
                src="images/sm_dogs.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?category=REPTILES&username=${sessionScope.username}"><img
                src="images/sm_reptiles.gif" />
        </a> <img
            src="images/separator.gif" />
        <a href="ViewCategory?category=CATS&username=${sessionScope.username}"><img
            src="images/sm_cats.gif" />
        </a> <img src="images/separator.gif" />
        <a href="ViewCategory?category=BIRDS&username=${sessionScope.username}"><img
                src="images/sm_birds.gif" />
        </a>
    </div>
</div>
<div id="Content">
